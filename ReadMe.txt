Projekt:	z1, TI1-P1 SS14
Organisation:	HAW-Hamburg, Informatik
Team:		S1T5
Autor(en):
Baha, Mir Farshid	(mirfarshid.baha@haw-hamburg.de)
G�tze, Nico		(nico.goetze@haw-hamburg.de)

Review History
==============
140408 v1.0 failed Schafers bug detected for 0, (1L<<48)-1
140410 v1.1 fixed bugs detected by Schafers, removed spaces to make him happy, changed the approach so there's no need for log anymore
