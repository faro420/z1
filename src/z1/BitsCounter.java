package z1;

/**
 * TeamNr:S1T5 <Mir Farshid, Baha> (<2141801>,<mirfarshid.baha@haw-hamburg.de>),
 * <Nico, G�tze> (<2125118>,<nico.goetze@haw-hamburg.de>),
 */
public class BitsCounter {
    public static void main(String[] args) {
        final long input = 9223372036854775807l;

        long n = input;
        int i = 0;
        if (n == 0)
            i++;
        while (n > 0) {
            i++;
            n = n >> 1;
        }
        System.out.println(i);
    }
}
